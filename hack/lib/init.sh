#!/usr/bin/env bash
#
#
# This script builds and link stamps the output

set -o errexit
set -o nounset
set -o pipefail

readonly PROJECT_MODULE=gitlab.com/lqshow/clireleaseautomator
readonly SOURCE_DATE_EPOCH=$(git show -s --format=format:%ct HEAD)

source "${PROJECT_ROOT}/hack/lib/version.sh"
