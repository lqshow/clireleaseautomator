#!/usr/bin/env bash
#
#
# This script builds and link stamps the output

set -o errexit
set -o nounset
set -o pipefail

readonly PROJECT_ROOT=$(dirname "${BASH_SOURCE[0]}")/../..
source "${PROJECT_ROOT}/hack/lib/init.sh"

VERBOSE=${VERBOSE:-"0"}
# V=""
if [[ "${VERBOSE}" == "1" ]];then
    # V="-x"
    set -x
fi

OUTPUT_DIR=${4:-"bin"}
BINARY_NAME=$(basename ${1})
BUILDPATH=./${1:?"path to build"}

BUILD_GOOS=${GOOS:-$(go env GOOS)}
BUILD_GOARCH=${GOARCH:-$(go env GOARCH)}
GOBINARY=${GOBINARY:-go}
LDFLAGS=$(version::ldflags)

if [ $# -ge 2 ] && [ -n $2 ]; then
  BUILD_GOOS=$2
fi

if [ $# -ge 3 ] && [ -n $3 ]; then
  BUILD_GOARCH=$3
fi

OUT=${OUTPUT_DIR}/${1:?"output path"}
if [ "${IS_RELEASE:-0}" == "1" ]; then
    OUT="${OUTPUT_DIR}/${BINARY_NAME}-${BUILD_GOOS}-${BUILD_GOARCH}"
    if [ "${BUILD_GOOS}" == "windows" ]; then
        OUT="${OUTPUT_DIR}/${BINARY_NAME}-${BUILD_GOOS}-${BUILD_GOARCH}.exe"
    fi
fi

# forgoing -i (incremental build) because it will be deprecated by tool chain.
GOOS=${BUILD_GOOS} CGO_ENABLED=0 GOARCH=${BUILD_GOARCH} ${GOBINARY} build \
        -ldflags="${LDFLAGS}" \
        -o "${OUT}" \
        "${BUILDPATH}"
