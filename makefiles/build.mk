##@ Build binaries

# Define a phony target for build-binaries to ensure it always runs
.PHONY: build-binaries

# Path to the build script
BUILD_SCRIPT_PATH := ./hack/gobuild/gobuild.sh

# List of binaries to build
BINARIES := cmd/fooctl cmd/barctl

# List of platforms and architectures to build for
ALLPLATFORMS := linux/amd64 linux/arm64 darwin/amd64 darwin/arm64 windows/amd64 windows/arm64

# Main target to build all combinations of binaries and platforms
build-binaries: $(foreach bin,$(BINARIES),$(foreach plat,$(ALLPLATFORMS),build-$(bin)-$(plat))) ## Build binaries

# Template for building each combination of binary and platform
define BUILD_template
build-$(1)-$(2):
	IS_RELEASE=1 $$(BUILD_SCRIPT_PATH) $(1) $$(subst /, ,$$(word 1,$$(subst -, ,$(2)))) $$(subst /, ,$$(word 2,$$(subst -, ,$(2))))
endef

# Generate build rules for each combination using the template
$(foreach bin,$(BINARIES),$(foreach plat,$(ALLPLATFORMS),$(eval $(call BUILD_template,$(bin),$(plat)))))