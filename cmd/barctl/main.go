package main

import (
	"os"

	"gitlab.com/lqshow/clireleaseautomator/cmd/barctl/command"
)

func main() {
	rootCmd := command.NewRootCommand()
	if err := rootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}
