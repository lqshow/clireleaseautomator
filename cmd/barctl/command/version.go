package command

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/lqshow/clireleaseautomator/version"
)

// NewCmdVersion returns a cobra command for fetching versions
func NewCmdVersion() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "version",
		Short: "Print the cli version information",
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Fprintf(os.Stdout, "Cli Version: %s\n", version.Get().GitVersion)
		},
	}

	return cmd
}
