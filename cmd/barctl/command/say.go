package command

import "github.com/spf13/cobra"

func NewCmdSay() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "say",
		Args:  cobra.MaximumNArgs(1),
		Short: "Say something",
		RunE: func(cmd *cobra.Command, args []string) error {
			s := "hello world"
			if len(args) > 0 {
				s = args[0]
			}
			cmd.Println(s)
			return nil
		},
	}
	return cmd
}
